readme.txt

############################################
		INTRODUCTION
############################################

Automaton is a BASH script which makes use of the expect package
( http://expect.sourceforge.net/ ) to automate sending and retrieving 
of files and execution of commands on a remote server/ servers.

Expect is a tool for automating interactive applications such as telnet, 
ftp, passwd, fsck, rlogin, tip, etc. Expect really makes this stuff 
trivial. Expect is also useful for testing these same applications.

############################################
		USE CASE
############################################

- organization does not allow passwordless logins to remote servers
- large numbers of servers requiring the same commands to be performed
- too lazy to perform repeat commands
- (insert your use case here)

############################################
		USAGE
############################################

If bash is your default shell, running the following command is sufficient
./automaton.sh 

Else use the following
sh automaton.sh 

############################################
		PREP-WORK
############################################

Automaton requires you to supply a list of remote servers in the following 
format :-

<FQDN/hostname> <IP Address>

File may be in either .txt or .csv format. Any greppable text format
is supported. Your mileage may vary.

############################################
		CAVEATS
############################################

- Passwords that start with the "$" are not supported.
- Some commands which uses regular expressions may not work properly

08242015
EOF
