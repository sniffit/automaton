#!/bin/bash
logdir=$PWD/automaton_log
logfile=$logdir/$1.$2.`date +%m%d%Y-%H:%M`
serverlist=$1
flag=$2

INFO () {
  echo "----------------------------------------------------------------------- " 
  echo "INFO: $@" 
  echo "----------------------------------------------------------------------- " 
}
if [ $# -lt 1 ]; then
        echo 
        echo "missing required parameter - no server list provided"
	echo "server list should be in the following format"
	echo "<server FQDN> <server IP>"
	echo "1 server per line - case insensitive"
	echo "Usage: $0 <server list> <options>"
	exit 1
fi
if [ $# -lt 2 ]; then
        echo 
        echo "missing required parameter - no options selected"
	echo "Usage: $0 <server list> <options>"
	echo "Valid options are :-"
	echo "upload_file     - uploads a file to remote server"
	echo "get_file        - downloads a file from remote server"
	echo "run_cmd         - runs a command on server"
	echo "run_cmd_sudo    - runs a command using sudo privileges on server"
	exit 1
fi

if [ ! -d $logdir ] ; then
mkdir $logdir
touch $logfile
fi

do_put_file () {
echo "Enter username and press [ENTER]"
read sshuser
echo "Enter password and press [ENTER]"
read -s sshpasswd
echo "Enter filename to upload  and press [ENTER]"
read upload_file
#echo "Enter target remote path to upload"
#read path_to_upload

runnerscr="put_file.exp"
cat $serverlist | while read FQDN IP; do
echo "Uploading $upload_file to $FQDN" | tee -a $logfile
spawner="scp $upload_file $sshuser@$IP:~/"
make_put_get
expect -f $runnerscr | tee -a $logfile
wait
echo "Upload done : $FQDN" | tee -a $logfile
rm -rf $runnerscr
done
}

do_get_file () {
echo "Enter target remote file"
read remote_path
echo "Enter destination local directory"
read local_path
echo "Enter username and press [ENTER]"
read sshuser
echo "Enter password and press [ENTER]"
read -s sshpasswd

runnerscr="get_file.exp"
cat $serverlist | while read FQDN IP; do
echo "Getting $remote_path from $FQDN " | tee -a $logfile
spawner="scp $sshuser@$IP:$remote_path $local_path"
make_put_get
expect -f $runnerscr | tee -a $logfile
wait
echo "Done getting file from $FQDN"  | tee -a $logfile
rm -rf $runnerscr
done
}

do_remote_cmd () {
echo -e "Enter username and press [ENTER]"
read sshuser
echo -e "Enter password and press [ENTER]"
read -s sshpasswd
echo -e "\nEnter remote command to execute and press [ENTER]\n( multiple commands seperated by & )"
read sshcmd

runnerscr="run_cmd.exp"
cat $serverlist | while read FQDN IP; do
echo "Connecting to $FQDN"  | tee -a $logfile
spawner="ssh -q $sshuser@$IP "
make_cmd_runner
echo "Running remote command on $FQDN" | tee -a $logfile
expect -f $runnerscr | tee -a $logfile
echo "Connection to $FQDN closed" | tee -a $logfile
rm -rf $runnerscr
done
}

do_remote_cmd_sudo () {
echo -e "Enter username and press [ENTER]"
read sshuser
echo -e "Enter password and press [ENTER]"
read -s sshpasswd
echo -e "\nEnter remote command to execute and press [ENTER]\n( multiple commands seperated by & )"
read sshcmd

runnerscr="run_cmd_sudo.exp"
cat $serverlist | while read FQDN IP; do
echo "Connecting to $FQDN"  | tee -a $logfile
spawner="ssh -q $sshuser@$IP "
make_cmd_runner
echo "Running remote command on $FQDN" | tee -a $logfile
expect -f $runnerscr | tee -a $logfile
echo "Connection to $FQDN closed" | tee -a $logfile
rm -rf $runnerscr
done
}


make_put_get () {
cat <<EOF >>$runnerscr
#!/usr/bin/expect -f 
set timeout 180
spawn $spawner
expect {
"*yes/no*" { send "yes\r";exp_continue }
"*assword:" { send "$sshpasswd\r";exp_continue }
}
EOF
}

make_cmd_runner () {
cat <<EOF >>$runnerscr
#!/usr/bin/expect -f 
set timeout 180
spawn $spawner
expect {
"*yes/no*" { send -- "yes\r";exp_continue}
"*assword:" { send -- "$sshpasswd\r";exp_continue}
-re {[:%#>?$~]} {
	expect -re {[:%#>?$~]} {send -- "$sshcmd\r";sleep 5;} 
	expect -re {[:%#>?$~]} {send -- "exit\r";exp_continue}
	} 
}
EOF
}


make_cmd_runner_sudo () {
cat <<EOF >>$runnerscr
#!/usr/bin/expect -f 
set timeout 180
spawn $spawner
expect {
"*yes/no*" { send -- "yes\r";exp_continue}
"*assword:" { send -- "$sshpasswd\r";exp_continue}
-re {[:%#>?$~]} { 
		send -- "sudo su -\r";
		expect "*assword*" { send -- "$sshpasswd\r";}
		expect -re {[:%#>?$~]} {send -- "$sshcmd\r";sleep 55555;}
		expect -re {[:%#>?$~]} {send -- "exit\r";exp_continue}
		}	
	}
EOF
}

# Main script body starts here

case $flag in 
	put_file)
	do_put_file
	INFO Retrieving error messages from $logfile
	cat $logfile | egrep -B3 -i 'error|failed|timed out|denied|disconnect'
	;;


	get_file)
	do_get_file
	INFO Retrieving error messages from $logfile
	cat $logfile | egrep -B3 -i 'error|failed|timed out|denied|disconnect'
	;;
	
	run_cmd)
	do_remote_cmd
	INFO Retrieving error messages from $logfile
	cat $logfile | egrep -B3 -i 'error|failed|timed out|denied|disconnect'
	;;
	
	run_sudo_cmd)
        do_remote_cmd_sudo
        INFO Retrieving error messages from $logfile
        cat $logfile | egrep -B3 -i 'error|failed|timed out|denied|disconnect'
        ;;	
	*)
	echo "BEEP!"
	exit 1
	;;

esac

exit 0
